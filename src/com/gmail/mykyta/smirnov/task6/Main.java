package com.gmail.mykyta.smirnov.task6;

public class Main {
    public static void main(String[] args) {
        triangleTwo(10);
    }

    public static void triangleTwo(int triangle){
        for (int i = 0; i < triangle; i++) {
            for (int j = triangle; j > i; j--) {
                System.out.print(" ");
            }

            for (int j1 = 0; j1 <= i; j1++) {
                if (j1 == 0 || j1 == i) {
                    System.out.print("*");
                } else {
                    if(i == triangle - 1){
                        System.out.print("*");
                    }else {
                        System.out.print(" ");
                    }
                }


            }
            System.out.println();
        }
    }
}
