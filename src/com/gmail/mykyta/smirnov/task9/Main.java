package com.gmail.mykyta.smirnov.task9;

public class Main {
    public static void main(String[] args) {
        triangleFive (10);
    }

    public static void triangleFive(int triangle){
        int middle = (int) (Math.ceil(triangle / 2));
        for(int i = 0; i < middle;i++){
            for (int j = 0; j < i; j++) {
                if(j == i-1 || j == 0) {
                    System.out.print("*");
                }else {
                    System.out.print(" ");
                }
            }

            for (int j = middle; j > i; j--) {
                System.out.print(" ");
            }

            System.out.println();
        }

        for (int i = middle; i >= 0; i--) {
            for (int j = 0; j < i; j++) {
                if(j == i - 1 || j == 0){
                    System.out.print("*");
                }else{
                    System.out.print(" ");
                }
            }

            for (int j = middle; j > i; j--) {
                System.out.print(" ");
            }

            System.out.println();
        }
    }
}
