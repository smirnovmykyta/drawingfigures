package com.gmail.mykyta.smirnov.task8;

public class Main {
    public static void main(String[] args) {
        triangleFour(10);
    }

    public static void triangleFour(int triangle){
        for (int i = triangle; i >= 0; i--) {

            for (int j = triangle; j > i; j--) {
                System.out.print(" ");
            }

            for (int j1 = 0; j1 <= i; j1++) {
                if (j1 == 0 || i == triangle || j1 == i) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
