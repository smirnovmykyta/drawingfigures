package com.gmail.mykyta.smirnov.task4;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws Exception {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

//        System.out.println("Пожалуйста введите не парное число:");
//        int lengthFigure = Integer.parseInt(bufferedReader.readLine());
        paintRhombus(9);

    }

    public static void paintRhombus(int lengthFigure) {
        int middle = (int) (Math.ceil(lengthFigure / 2));

        for (int i = 0; i < middle; i++) {

            for (int j = middle; j > i; j--) {
                System.out.print(" ");
            }

            for (int j1 = 0; j1 <= i; j1++) {
                if (j1 == 0) {
                    System.out.print("*");
                }else {
                    System.out.print(" ");
                }
            }

            for (int j = 0; j < i; j++) {
                if(j == i-1) {
                    System.out.print("*");
                }else {
                    System.out.print(" ");
                }
            }

            for (int j = middle; j > i; j--) {
                System.out.print(" ");
            }

            System.out.println();
        }
        for (int i = middle; i >= 0; i--) {

            for (int j = middle; j > i; j--) {
                System.out.print(" ");
            }

            for (int j1 = 0; j1 <= i; j1++) {
                if(j1 == 0) {
                    System.out.print("*");
                }else{
                    System.out.print(" ");
                }
            }

            for (int j = 0; j < i; j++) {
                if(j == i - 1){
                System.out.print("*");
                }else{
                    System.out.print(" ");
                }
            }

            for (int j = middle; j > i; j--) {
                System.out.print(" ");
            }

            System.out.println();
        }
    }
}

