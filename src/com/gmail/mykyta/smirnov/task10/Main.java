package com.gmail.mykyta.smirnov.task10;

public class Main {
    public static void main(String[] args) {
        triangleSix(10);
    }

    public static void triangleSix(int length){
        int middle = (int) (Math.ceil(length / 2));
        for (int i = 0; i < middle; i++) {
            for (int j = middle; j > i; j--) {
                System.out.print(" ");
            }

            for (int j1 = 0; j1 <= i; j1++) {
                if (j1 == 0 || j1 == i) {
                    System.out.print("*");
                }else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }

        for (int i = middle; i >= 0; i--) {
            for (int j = middle; j > i; j--) {
                System.out.print(" ");
            }

            for (int j1 = 0; j1 <= i; j1++) {
                if(j1 == 0 || j1 == i) {
                    System.out.print("*");
                }else{
                    System.out.print(" ");
                }
            }
            System.out.println();
        }

    }
}
