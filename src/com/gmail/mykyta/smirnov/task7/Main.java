package com.gmail.mykyta.smirnov.task7;

public class Main {
    public static void main(String[] args) {

        triangleThree(10);
    }

    public static void triangleThree(int triangle){
        for(int i = triangle; i > 0; i--){
            for(int j = 0; j<= i; j++){
                if(j == 0 || j == i){
                    System.out.print("*");
                }else{
                    if(i != triangle ) {
                        System.out.print(" ");
                    }else{
                        System.out.print("*");
                    }
                }
            }
            System.out.println();
            if(i == 1){
                System.out.println("*");
            }
        }
    }
}
