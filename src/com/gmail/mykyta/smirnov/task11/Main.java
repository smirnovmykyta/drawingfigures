package com.gmail.mykyta.smirnov.task11;

public class Main {
    public static void main(String[] args) {
        triangleSeven(10);
    }

    public static void triangleSeven(int height){

        for (int i = 0; i < height; i++) {

            for (int j = height; j > i; j--) {
                System.out.print(" ");
            }

            for (int j1 = 0; j1 <= i; j1++) {
                if (j1 == 0 || i == height - 1) {
                    System.out.print("*");
                }else {
                    System.out.print(" ");
                }
            }

            for (int j = 0; j < i; j++) {
                if(j == i-1 || i == height - 1) {
                    System.out.print("*");
                }else {
                    System.out.print(" ");
                }
            }

            for (int j = height; j > i; j--) {
                System.out.print(" ");
            }

            System.out.println();
        }
    }
}
