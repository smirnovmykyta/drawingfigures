package com.gmail.mykyta.smirnov.task3;

public class Main {
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (i == 0 || i == 9) {
                    if (j == 3 || j == 4 || j == 5 || j == 6) {
                        System.out.print("*");
                    } else System.out.print(" ");
                } else if (i == 8 || i == 1) {
                    if (j == 2 || j == 7) {
                        System.out.print("*");
                    } else System.out.print(" ");
                } else if (i == 2 || i == 7) {
                    if (j == 1 || j == 8) {
                        System.out.print("*");
                    } else System.out.print(" ");
                } else if (i == 3 || i == 4 || i == 5 || i == 6) {
                    if (j == 0 || j == 9) {
                        System.out.print("*");
                    } else System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
