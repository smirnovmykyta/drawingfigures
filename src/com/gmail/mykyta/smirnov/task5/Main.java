package com.gmail.mykyta.smirnov.task5;

public class Main {
    public static void main(String[] args) {

        triangleOne(10);
    }

    public static void triangleOne(int triangle){
        for(int i = 0; i < triangle; i++){
            for(int j = 0; j<= i; j++){
                if(j == 0 || j == i){
                    System.out.print("*");
                }else{
                    if(i != triangle -1) {
                        System.out.print(" ");
                    }else{
                        System.out.print("*");
                    }
                }
            }
            System.out.println();
        }
    }
}
